
###To build: run with:
```
mvn clean install (Java 8)
```

###To run : in the target folder, run:
```
java -jar eBillingDataFileSplitter-1.0-SNAPSHOT.jar
```