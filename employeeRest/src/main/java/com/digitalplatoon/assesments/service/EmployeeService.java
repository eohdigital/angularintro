package com.digitalplatoon.assesments.service;

import com.digitalplatoon.assesments.employee.EmployeeDTO;
import com.digitalplatoon.assesments.employee.EmployeeEntity;
import com.digitalplatoon.assesments.repo.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

  @Autowired
  private EmployeeRepository employeeRepository;

  public EmployeeDTO getEmployee(long id) {
    EmployeeEntity entity = employeeRepository.findOne(id);
    if (entity != null) {
      return new EmployeeDTO(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getIdNumber());
    } else {
      return null;
    }
  }

  public List<EmployeeDTO> getAllEmployees() {
    Iterable<EmployeeEntity> entities = employeeRepository.findAll();
    List<EmployeeDTO> employeeDTOs = new ArrayList();
    if (entities != null) {
      for (EmployeeEntity entity : entities) {
        employeeDTOs.add(new EmployeeDTO(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getIdNumber()));
      }
    }
    return employeeDTOs;
  }

  public EmployeeDTO addEmployee(EmployeeDTO employeeDTO) {
    EmployeeEntity entity = new EmployeeEntity(employeeDTO.getFirstName(), employeeDTO.getLastName(), employeeDTO.getIdNumber());
    if (employeeRepository.save(entity) != null) {
      return new EmployeeDTO(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getIdNumber());
    } else {
      return null;
    }
  }

  public List<EmployeeDTO> deleteEmployee(long id) {
    EmployeeEntity entity = employeeRepository.findOne(id);
    employeeRepository.delete(entity);
    return getAllEmployees();
  }

  public boolean validateSAIdNumber(String idNumber) {

    long oddNumberSum = 0;
    long evenNums = 0;

    try {

//Getting the sum of odd numbers excluding the last digit
      for (int i = 0; i < idNumber.length() - 1; i += 2) {
        oddNumberSum += Long.parseLong(String.valueOf(idNumber.charAt(i)));
      }

//Getting all the even numbers
      StringBuilder evenNumsStr = new StringBuilder();
      for (int i = 1; i < idNumber.length(); i += 2) {
        evenNumsStr.append(idNumber.charAt(i));
      }

      evenNums = Long.parseLong(evenNumsStr.toString().trim());

      long dblEvenNums = evenNums * 2;

      long dblEvensSum = 0;
      for (int i = 0; i < String.valueOf(dblEvenNums).length(); i++) {
        dblEvensSum += Long.parseLong(String.valueOf(String.valueOf(dblEvenNums).charAt(i)));
      }

      long oddsPlusDblEvens = oddNumberSum + dblEvensSum;

      long controlDigitCheck = 10 - Long.parseLong(String.valueOf(oddsPlusDblEvens).substring(String.valueOf(oddsPlusDblEvens).length() - 1));

      if (controlDigitCheck == 10) {
        controlDigitCheck = 0;
      }

      if (controlDigitCheck != Long.parseLong(idNumber.substring(idNumber.length() - 1))) {
        return false;
      } else {
        return true;
      }
    } catch (NumberFormatException e) {
      return false;
    }
  }

}
