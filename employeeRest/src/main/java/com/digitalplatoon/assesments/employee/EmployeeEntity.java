package com.digitalplatoon.assesments.employee;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "employee")
public class EmployeeEntity {

  public EmployeeEntity() {
  }

  public EmployeeEntity(String firstName, String lastName, String idNumber) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.idNumber = idNumber;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column
  public long id;

  @Column
  public String firstName;

  @Column
  public String lastName;

  @Column
  public String idNumber;

}
