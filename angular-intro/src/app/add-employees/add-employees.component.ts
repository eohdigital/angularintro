import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AddEmployeeService} from './add-employee.service';

@Component({
  selector: 'app-add-employees',
  templateUrl: './add-employees.component.html',
  styleUrls: ['./add-employees.component.css']
})
export class AddEmployeesComponent implements OnInit {

  employeeForm: FormGroup;
  submitted: boolean = false;

  constructor(private fb: FormBuilder, private addEmployeeService: AddEmployeeService) {}

  ngOnInit() {

    this.employeeForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern("[a-zA-Z ]*")]],
      lastName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern("[a-zA-Z ]*")]],
      idNumber: ['', [Validators.required, Validators.minLength(13), Validators.maxLength(13), Validators.pattern("[0-9]*")]]
    });
  }
  
  addAnother() {
    this.submitted= false;
    this.employeeForm.reset();
  }


  onSubmit(value: string, isValid: boolean) {

    if (isValid) {
      this.addEmployeeService.addEmployee(value).subscribe(
        (res) => {
          this.submitted= true;
          localStorage.setItem("newEmployee", "true");
        },
        (error) => {
          console.log("error");
        });

    } else {
      return false;
    }
  }

}

