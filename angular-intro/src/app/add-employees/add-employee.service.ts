import {Injectable} from '@angular/core';
import {Employee} from '../shared/Employee';
import {Observable} from 'rxjs/Observable';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw'

@Injectable()
export class AddEmployeeService {

  constructor(private http: Http) {}

  addEmployee(value): Observable<Employee> {
    let url = "http://localhost:8080/app/employee/";

    return this.http.post(url, value)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
