import { Injectable } from '@angular/core';
import { Employee } from '../shared/Employee';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw'

@Injectable() 
export class EmployeesService {

  constructor(private http: Http) { }

  getEmployees(): Observable<Array<Employee>> {
    let url = "http://localhost:8080/app/employee/";

    return this.http.get(url)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  
   fireSomeone(employeeId): Observable<Array<Employee>> {
    let url = "http://localhost:8080/app/employee/" + employeeId;
    console.log(url);
    return this.http.delete(url)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}


