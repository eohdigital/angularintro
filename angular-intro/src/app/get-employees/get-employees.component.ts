import {Component, OnInit} from '@angular/core';
import {EmployeesService} from './get-employees.service';
import {Employee} from '../shared/Employee';

@Component({
  selector: 'app-get-employees',
  templateUrl: './get-employees.component.html',
  styleUrls: ['./get-employees.component.css']
})
export class GetEmployeesComponent implements OnInit {

  employeeList: Employee[] = [];

  constructor(
    private employeesService: EmployeesService
  ) {}

  ngOnInit() {
    this.getAllEmployees();
  }

  getAllEmployees() {
    this.employeesService.getEmployees().subscribe(
      (res) => {
        this.employeeList = res;
      },
      (error) => {
        console.log(error.json);
      });
  }
  
  fireSomeone(employeeId) {
    this.employeesService.fireSomeone(employeeId).subscribe(
      (res) => {
        this.employeeList = res;
      },
      (error) => {
        console.log(error.json);
      });
  }

}
