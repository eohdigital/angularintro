import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GetEmployeesComponent } from './get-employees/get-employees.component';
import { AppRoutingModule } from './app-routing.module';
import { AddEmployeesComponent } from './add-employees/add-employees.component';
import { EmployeesService } from './get-employees/get-employees.service';
import { AddEmployeeService } from './add-employees/add-employee.service';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    GetEmployeesComponent,
    AddEmployeesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
  EmployeesService,
  AddEmployeeService

],
  bootstrap: [AppComponent]
})
export class AppModule { }
