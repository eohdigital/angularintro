import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GetEmployeesComponent }      from './get-employees/get-employees.component';
import { AddEmployeesComponent }      from './add-employees/add-employees.component';

const routes: Routes = [
  { path: 'get-employees', component: GetEmployeesComponent },
  { path: 'add-employees', component: AddEmployeesComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { 
}


