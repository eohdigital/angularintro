package com.digitalplatoon.assesments.employee;

import lombok.Data;

@Data
public class EmployeeDTO {

  public long id;
  public String firstName;
  public String lastName;
  public String idNumber;

  public EmployeeDTO(long id, String firstName, String lastName, String idNumber) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.idNumber = idNumber;
  }

  public EmployeeDTO() {
  }

}
