package com.digitalplatoon.assesments.service;

import com.digitalplatoon.assesments.employee.EmployeeDTO;
import com.digitalplatoon.assesments.employee.EmployeeEntity;
import com.digitalplatoon.assesments.repo.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

  @Autowired
  private EmployeeRepository employeeRepository;

  public EmployeeDTO getEmployee(long id) {
    EmployeeEntity entity = employeeRepository.findOne(id);
    if (entity != null) {
      return new EmployeeDTO(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getIdNumber());
    } else {
      return null;
    }
  }

  public List<EmployeeDTO> getAllEmployees() {
    Iterable<EmployeeEntity> entities = employeeRepository.findAll();
    List<EmployeeDTO> employeeDTOs = new ArrayList();
    if (entities != null) {
      for (EmployeeEntity entity : entities) {
        employeeDTOs.add(new EmployeeDTO(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getIdNumber()));
      }
    }
    return employeeDTOs;
  }

  public EmployeeDTO addEmployee(EmployeeDTO employeeDTO) {
    EmployeeEntity entity = new EmployeeEntity(employeeDTO.getFirstName(), employeeDTO.getLastName(), employeeDTO.getIdNumber());
    if (employeeRepository.save(entity) != null) {
      return new EmployeeDTO(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getIdNumber());
    } else {
      return null;
    }
  }

  public List<EmployeeDTO> deleteEmployee(long id) {
    EmployeeEntity entity = employeeRepository.findOne(id);
    employeeRepository.delete(entity);
    return getAllEmployees();
  }

}
