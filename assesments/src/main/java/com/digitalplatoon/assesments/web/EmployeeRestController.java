package com.digitalplatoon.assesments.web;

import com.digitalplatoon.assesments.employee.EmployeeDTO;
import com.digitalplatoon.assesments.service.EmployeeService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Rest methods used for employee management")
public class EmployeeRestController {

  @Autowired
  private EmployeeService employeeService;

  @CrossOrigin
  @GetMapping("/employee/{id}")
  @ApiOperation(value = "Rest method used for retrieving an employee")
  public ResponseEntity<EmployeeDTO> getEmployee(@PathVariable("id") long id) {
    EmployeeDTO dto = employeeService.getEmployee(id);

    if (dto != null) {
      return new ResponseEntity<EmployeeDTO>(dto, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

  }

  @CrossOrigin
  @GetMapping("/employee/")
  @ApiOperation(value = "Rest method used for retrieving all employees")
  public ResponseEntity<List<EmployeeDTO>> getEmployees() {
    List<EmployeeDTO> dtos = employeeService.getAllEmployees();

    if (dtos != null) {
      return new ResponseEntity<List<EmployeeDTO>>(dtos, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

  }
  
  @CrossOrigin
  @DeleteMapping("/employee/{id}")
  @ApiOperation(value = "Rest method used for retrieving all employees")
  public ResponseEntity<List<EmployeeDTO>> deleteEmployee(@PathVariable("id") long id) {
    List<EmployeeDTO> dtos = employeeService.deleteEmployee(id);

    if (dtos != null) {
      return new ResponseEntity<List<EmployeeDTO>>(dtos, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

  }

  @CrossOrigin
  @PostMapping("/employee")
  @ApiOperation(value = "Rest method used for adding an employee")
  public ResponseEntity<EmployeeDTO> addEmployee(@RequestBody EmployeeDTO employeeDto) {
    EmployeeDTO dto = employeeService.addEmployee(employeeDto);
    if (dto != null) {
      return new ResponseEntity<EmployeeDTO>(dto, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
