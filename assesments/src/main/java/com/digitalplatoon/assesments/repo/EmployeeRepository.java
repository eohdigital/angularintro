package com.digitalplatoon.assesments.repo;

import com.digitalplatoon.assesments.employee.EmployeeEntity;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Serializable> {

}
