package com.digitalplatoon.assesments;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AssesmentsApplicationTests.class})
public class AssesmentsApplicationTests {

  @Test
  public void contextLoads() {
  }

}
